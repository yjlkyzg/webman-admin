# webman 后台管理 laravel 版

[![Latest Stable Version](http://poser.pugx.org/le/webman-admin/v)](https://packagist.org/packages/le/webman-admin) [![Total Downloads](http://poser.pugx.org/le/webman-admin/downloads)](https://packagist.org/packages/le/webman-admin) [![Latest Unstable Version](http://poser.pugx.org/le/webman-admin/v/unstable)](https://packagist.org/packages/le/webman-admin) [![License](http://poser.pugx.org/le/webman-admin/license)](https://packagist.org/packages/le/webman-admin) [![PHP Version Require](http://poser.pugx.org/le/webman-admin/require/php)](https://packagist.org/packages/le/webman-admin)

#### 介绍
webman + pearadmin 后台管理框架(laravel版)<br/>
由于thinkphp的很多组件都不单独维护，所以用laravel组件再实现一遍<br/>
该项目相比之前的 [webman + pearadmin 后台管理框架](https://gitee.com/yjlkyzg/webmanadmin/tree/master) 主要改用laravel组件，使用php8.0以上版本,简化后台缓存内容<br/>

已完成登录、菜单、权限、角色、管理员、图片上传功能

#### 软件架构
1.  后端使用基于workerman的webnman http常驻内存框架 + Php8.0 + Mysql8.0 + Redis
2.  使用laravel的Eloquent ORM 、laravel-cache、laravel-redis
2.  前端使用pear admin layui 版本

#### 开发说明

1.  使用success() error() 统一返回成功、失败相应 方法在app\function.php中
2.  使用Logger类进行日志记录,默认设置info、error、debug、admin、api、request目录,例:Logger::info('测试日志信息')
3.  新增统一异常处理类 support/ExceptionHandler.php 统一返回json错误信息，记录日志到error目录下
4.  app/exception为自定义的异常类，可根据业务增加
5.  app/middleware为中间件目录,Access.php为全局中间件，处理跨域等
6.  使用webman/log记录请求日志,默认关闭,启用请前往config/plugin/webman/log/app.php,会记录所有请求数据以及sql等,生产务必要关闭，否则会产生大量日志文件,系统Access.php中间件提供一个队列(app/queue/redis/fast/RequestLog.php)处理请求日志，默认已注释，如果需要可以开启该队列(取消Access中的注释)，改造为记录到mongodb或mysql即可
7.  function.php 部分方法, get_image_url 获取可访问的文件路径 get_option_value 获取配置数组 get_option_key 获取配置下某个key get_admin_id 获取当前管理员id
8.  目前后台缓存数据，管理员信息、角色的菜单ids、角色的菜单列表、系统配置数据


开发说明后续持续更新...


#### 使用组件(开发中有问题可查看对应组件文档)

1.  [illuminate/database laravel数据库组件](https://www.workerman.net/doc/webman/db/tutorial.html)
2.  [illuminate/redis laravel redis](https://www.workerman.net/doc/webman/db/redis.html)
3.  [vlucas/phpdotenv env环境变量组件](https://www.workerman.net/doc/webman/components/env.html)
4.  [workerman/validation 验证器](https://www.workerman.net/doc/webman/components/validation.html)
5.  [webman/captcha 验证码](https://www.workerman.net/doc/webman/components/captcha.html)
6.  [workerman/crontab 定时任务](https://www.workerman.net/doc/webman/components/crontab.html)
7.  [phpoffice/phpspreadsheet Excel表格](https://www.workerman.net/doc/webman/components/excel.html)
8.  [webman/redis-queue redis队列](https://www.workerman.net/plugin/12)
9.  [webman/event event事件](https://www.workerman.net/plugin/64)
10.  [webman-tech/logger 日志统筹化(分目录生成日志)](https://www.workerman.net/plugin/58)
11.  [shopwwi/laravel-cache laravel缓存](https://www.workerman.net/plugin/95)
12.  [tinywan/storage 文件上传(本地、阿里云、腾讯云、七牛云)](https://www.workerman.net/plugin/21)
13.  [topthink/think-template Thinkphp模板引擎](https://www.kancloud.cn/manual/think-template/1286403)
14.  [webman/log 日志记录(默认关闭，开启请前往config/plugin/webman/log/app.php)](https://www.workerman.net/plugin/61)


#### 安装教程

1.  安装环境，可使用宝塔安装，Php8.0 + Mysql8.0 + Redis , php安装redis扩展，删除宝塔禁用函数
2.  使用composer 安装 composer require le/webman-admin
3.  导入/sql 文件
4.  复制.env.example 并重命名为.env,修改里面的mysql、redis配置
5.  项目根目录使用 php start.php start 启动 加上 -d 则为保持后台运行(首次建议不加-d 可查看是否有报错)

##### V1.1
1.  后台增加上传附件列表


##### V1.0
1.  完成基本功能,后台登录、系统设置、菜单管理、角色管理、管理员管理、修改密码、图片上传功能


#### 部分截图
![输入图片说明](https://www.itjiale.com/images/1.png)
![输入图片说明](https://www.itjiale.com/images/2.png)
![输入图片说明](https://www.itjiale.com/images/3.png)
![输入图片说明](https://www.itjiale.com/images/4.png)
![输入图片说明](https://www.itjiale.com/images/5.png)