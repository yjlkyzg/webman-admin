<?php

use app\admin\event\AdminEvent;
use app\model\AdminModel;

return [
    'admin.admin_upd_cache' => [//更新管理员缓存
        function($data){
            $model = new AdminModel();
            $model->getDetail($data,true);
        }
    ],
];
