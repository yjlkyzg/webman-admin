<?php
return [
    //快速消费队列
    'consumer_fast'  => [
        'handler'     => Webman\RedisQueue\Process\Consumer::class,
        'count'       => 8, // 可以设置多进程同时消费
        'constructor' => [
            // 消费者类目录
            'consumer_dir' => app_path() . '/queue/redis/fast'
        ]
    ],
    //扩展其他消费队列
    // 'consumer_other'  => [
    //     'handler'     => Webman\RedisQueue\Process\Consumer::class,
    //     'count'       => 8, // 可以设置多进程同时消费
    //     'constructor' => [
    //         // 消费者类目录
    //         'consumer_dir' => app_path() . '/queue/other'
    //     ]
    // ]
];
