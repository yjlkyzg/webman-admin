<?php

namespace app\admin\controller;

use app\model\AdminMenuModel;
use app\model\AdminModel;
use app\model\UploadFileModel;
use app\service\upload\UploadService;
use Shopwwi\LaravelCache\Cache;
use support\Db;
use support\Redis;
use support\Request;
use support\View;

class IndexController
{

    /**
     * 后台框架
     */
    public function index(Request $request)
    {
        return view('index/index');
    }


    /**
     * 后台首页
     */
    public function main(Request $request)
    {
        //获取最近七天的请求量
        $date_arr = [];
        $pv_arr = [];

        for ($i = 6; $i >= 0; $i--) {
            $a = strtotime(date("Y-m-d 00:00:00", strtotime("- $i day")));

            $date_arr[] = date('Y-m-d', $a);
            $pv_arr[] = Cache::get('request_pv_' . $a) ?? 0;
        }

        //今日请求量
        $day_pv = Cache::get('request_pv_' . strtotime(date('Y-m-d 00:00:00')));

        //cpu核心数量
        $cpu_count = cpu_count();

        //启用进程数量
        $start_server = config('server.count');

        //站点总请求量
        $total_pv = Cache::get('request_pv_total');

        //上传附件配置
        $upload = bcdiv(config('server.max_package_size') / 1024, 1024, 0);

        //获取已上传附件总量
        $uploadsize = UploadFileModel::sum('size') ?? 0;
        $uploadsize = sprintf("%.3f", $uploadsize / 1024 / 1024) . 'M';

        //$os
        $os = PHP_OS;

        $mysql = Db::select('SELECT VERSION() as mysql_version')[0]->mysql_version;

        //返回redis信息
        $handler = Redis::connection('default');
        $redis = $handler->info();

        //获取当前db的key总量
        $dbinfo = $redis['db' . getenv('RDS_DATABASE')];
        $keys = end(explode("=", explode(",", $dbinfo)[0]));

        $redis['keycount'] = $keys;

        return view('index/main', [
            'os' => $os,
            'mysql' => $mysql,
            'redis' => $redis,
            'upload' => $upload,
            'uploadsize' => $uploadsize,
            'php_version' => PHP_VERSION,
            'day_pv' => $day_pv,
            'cpu_count' => $cpu_count,
            'start_server' => $start_server,
            'total_pv' => $total_pv,
            'date_arr' => json_encode($date_arr),
            'pv_arr' => json_encode($pv_arr),
        ]);
    }

    /**
     * 后台管理员获取菜单
     */
    public function getMenu(Request $request){

        $admin_id = get_admin_id();

        //增加缓存 获取角色,查询角色下的菜单列表
        $list = Cache::tags('admin_rules')->get('admin_rules_list_'.$admin_id);

        if(empty($list) || true){

            $adminModel = new AdminModel();
            $rules = $adminModel->getRulesIds($admin_id);

            $menuModel = new AdminMenuModel();

            if(in_array("*",$rules)){
                //超级管理员
                $list =  $menuModel->where("is_show",1)->orderBy("sort","desc")->get(['id','pid','title','icon','type','app','controller','action','is_show','sort'])->mapWithKeys(function ($item) {
                    return [$item->id => $item];
                });
            }else{
                $list =  $menuModel->whereIn("id",$rules)->where("is_show",1)->orderBy("sort","desc")->get(['id','pid','title','icon','type','app','controller','action','is_show','sort'])->mapWithKeys(function ($item) {
                    return [$item->id => $item];
                });
            }

            if($list->isEmpty()){
                return [];
            }
            
            $list = $list->toArray();

            //处理上下级层级
            foreach($list as $k=>$v){
                $list[$k]['icon'] = 'layui-icon '.$v['icon'];
                $list[$k]['openType'] = '_iframe';
                $list[$k]['href'] = '/'.$v['app'].'/'.$v['controller'].'/'.$v['action'];
                if($v['pid'] && isset($list[$v['pid']])){
                    $list[$v['pid']]['children'][] = &$list[$k];
                }
            }

            //将最顶层的id下表去除,放到最外面一层
            $rtnlist = [];
            foreach($list as $k=>$v){
                if(!$v['pid']){
                    $rtnlist[] = $v;
                }
            }

            $list = $rtnlist;

            Cache::tags('admin_rules')->set('admin_rules_list_'.$admin_id,$rtnlist,86400 * 30);
        }
        
        return json($list);
    }

    //后台文件上传
    public function upload(Request $request){

        $upload = new UploadService();

        try{
            $res = $upload->upload($request->file());
        }catch(\Throwable $throwable){

            return error($throwable->getMessage());
        }

        return success('ok',$res);
    }

    //退出登录
    public function logout(Request $request){
        $request->session()->set('admin_id',0);
        return success('退出成功');
    }

}
