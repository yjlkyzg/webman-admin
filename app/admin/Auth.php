<?php
namespace app\admin;

use app\model\AdminMenuModel;
use app\model\AdminModel;

//鉴权类
class Auth
{
    /**
     * 验证是否有权限
     */
    static function canAccess($admin_id,$controller,$action,$app){

        //获取管理员所有菜单权限

        $adminModel = new AdminModel();

        //用户角色
        $rules = $adminModel->getRulesIds($admin_id);

        if(empty($rules)) return false;

        //判断超级管理员
        if(in_array('*',$rules)){
            return true;
        }

        //格式化controller
        $controller = str_replace('Controller','',end(explode("\\",$controller)));

        //判断权限是否添加到数据库
        $menu_id = AdminMenuModel::where([
            ['action','=',$action],
            ['controller','=',$controller],
            ['app','=',$app]
        ])->value("id");

        //未添加的权限，默认放行
        if(!$menu_id) return true;

        //验证权限
        if(in_array($menu_id,$rules)){
            return true;
        }

        return false;
    }


}