<?php

namespace app\middleware;

use Shopwwi\LaravelCache\Cache;
use Webman\MiddlewareInterface;
use Webman\Http\Response;
use Webman\Http\Request;
use Webman\RedisQueue\Client;

/**
 * 全局中间件
 */
class Access implements MiddlewareInterface
{
    public function process(Request $request, callable $next): Response
    {
        $header = [
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Allow-Origin' => $request->header('origin', '*'),
            'Access-Control-Allow-Methods' => $request->header('access-control-request-method', '*'),
            'Access-Control-Allow-Headers' => $request->header('access-control-request-headers', '*'),
        ];

        //判断是否是options请求
        if ($request->method() == 'OPTIONS') {
            return response('', 200, $header);
        }

        //组合请求记录数据
        $request_log = [];
        $start_time = microtime(true);
        $request_log['ip'] = $request->getRealIp();
        $request_log['method'] = $request->method();
        $request_log['fullurl'] = trim($request->fullUrl(), '/');
        $request_log['app'] = $request->app;
        $request_log['controller'] = end(explode("\\", $request->controller));
        $request_log['action'] = $request->action;
        $request_log['route'] = $request->route ? $request->route->getPath() : '';

        //今日请求量增加
        $request_pv_cache_key = 'request_pv_' . strtotime(date('Y-m-d 00:00:00'));
        Cache::increment($request_pv_cache_key);

        //站点总请求量
        Cache::increment('request_pv_total');

        //继续执行，并得到一个响应
        $response = $next($request);

        //请求耗时,ms
        $request_log['times'] = sprintf("%.3f", (microtime(true) - $start_time) * 1000);


        // 给响应添加跨域相关的http头
        $response->withHeaders($header);

        $exception = $response->exception();
        if ($exception) {
            //捕获异常
            $request_log['exception'] = $exception->getMessage();
        }

        //请求记录队列
        //Client::send('request_log', $request_log);

        return $response;
    }
}
