<?php

namespace app\middleware;

use app\admin\Auth;
use support\View;
use Webman\MiddlewareInterface;
use Webman\Http\Response;
use Webman\Http\Request;

/**
 * 后台鉴权中间价
 */
class Admin implements MiddlewareInterface
{
    public function process(Request $request, callable $next): Response
    {
        $controller = $request->controller;
        $action = $request->action;
        $app = $request->app;

        if ($controller != 'app\admin\controller\PublicController') {

            //管理员鉴权,验证登录、权限、返回权限菜单
            $admin_id = get_admin_id();
            if (empty($admin_id)) {

                if ($request->expectsJson()) {
                    return error('请先登录', '', [], 10001);
                }

                return redirect('/admin/public/login');
            }

            //TODO 验证管理员禁用状态

            //权限验证
            if (!Auth::canAccess($admin_id, $controller, $action, $app)) {
                return error('没有访问权限!');
            }
        }

        return $next($request);
    }
}
