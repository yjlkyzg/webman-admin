<?php

namespace app\model;

/**
 * 配置模型
 */
class OptionModel extends BaseModel
{

    protected $table = 'option';

    protected $casts_childs = [
        'option_value'=>'array'
    ];
    
    public function __construct() {
        $this->casts = array_merge($this->casts_childs, $this->casts);
    }

}
